#!/bin/bash

# ------------------------------------------------------------------------------
# Run Sphinx in live HTML mode. Sphinx will watch the project directory and
# rebuild the documentation when a change is detected. The documentation will
# be available at http://localhost:<port>. The default port is 8000.
#
# Examples:
#   ./make-livehtml
#   ./make-livehtml --name my-documentation --interface 0.0.0.0 --port 9000
# ------------------------------------------------------------------------------

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_DIR="$(realpath "$SCRIPT_DIR/..")"

# shellcheck disable=SC1090
source "$SCRIPT_DIR/variables.sh"
# shellcheck disable=SC1090
source "$SCRIPT_DIR/commons.sh"

function fatal() {
    print_fatal "$1"
    help
    exit 1
}

function main() {

    print_line
    print_h1 "Building Live HTML documentation"
    print_line

    local options_index=1

    local build_dir="$PROJECT_DIR/build"
    local source_dir="$PROJECT_DIR/source"

    # shellcheck disable=SC2153
    local sphinx_docker_image="ddidier/sphinx-doc:$SPHINX_DOCKER_IMAGE_VERSION"
    # shellcheck disable=SC2153
    local sphinx_docker_expose_interface=$SPHINX_DOCKER_EXPOSE_INTERFACE
    # shellcheck disable=SC2153
    local sphinx_docker_expose_port=$SPHINX_DOCKER_EXPOSE_PORT
    # shellcheck disable=SC2153
    local sphinx_docker_name=$SPHINX_DOCKER_NAME

    while [[ $options_index -le $# ]]; do

        case "${!options_index}" in
            -i|--interface)
                options_index=$((options_index + 1))
                [[ $options_index -le $# ]] || fatal "Missing exposed interface  value"
                sphinx_docker_expose_interface="${!options_index}"
                ;;
            -n|--name)
                options_index=$((options_index + 1))
                [[ $options_index -le $# ]] || fatal "Missing name value"
                sphinx_docker_name="${!options_index}"
                ;;
            -p|--port)
                options_index=$((options_index + 1))
                [[ $options_index -le $# ]] || fatal "Missing exposed port value"
                sphinx_docker_expose_port="${!options_index}"
                ;;
            -h|--help)
                help
                exit 0
                ;;
            *)
                fatal "Invalid argument '${!options_index}'"
                ;;
        esac

        options_index=$((options_index + 1))
    done

    if [[ -z "$sphinx_docker_expose_interface" ]]; then
        fatal "The exposed interface cannot be empty. Please use '-i' or '--interface'."
    fi

    if [[ -z "$sphinx_docker_name" ]]; then
        fatal "The name cannot be empty. Please use '-n' or '--name'."
    fi

    if [[ -z "$sphinx_docker_expose_port" ]]; then
        fatal "The exposed port cannot be empty. Please use '-p' or '--port'."
    fi

    print_h1_highlight "Docker image name......... $sphinx_docker_image"
    print_h1_highlight "Source directory ......... $source_dir"
    print_h1_highlight "Build directory .......... $build_dir"
    print_h1_highlight "User ID .................. $UID"
    print_h1_highlight ""
    print_h1_highlight "Docker container name .... $sphinx_docker_name"
    print_h1_highlight "Listening on interface ... $sphinx_docker_expose_interface"
    print_h1_highlight "Listening on port ........ $sphinx_docker_expose_port"
    print_line
    print_information_box "Container can be terminated with:"
    print_line
    print_information_highlight "By default: Control+C"
    print_information_highlight "If not responding: docker kill $sphinx_docker_name"
    print_line

    print_step_information "Deleting build directory '$build_dir'"
    rm -rf "$build_dir"
    print_line

    print_step_information "Running Sphinx image '$sphinx_docker_image'"
    print_line
    start_output_section
    print_line

    local ignore_files=""
    for ignore_file in "${SPHINX_DOCKER_IGNORE_FILES[@]}"; do
        ignore_files="${ignore_files} --ignore \"$ignore_file\""
    done

    docker run --rm -it \
        -e USER_ID=$UID \
        --name $sphinx_docker_name \
        -p $sphinx_docker_expose_interface:$sphinx_docker_expose_port:$sphinx_docker_expose_port \
        -v "$PROJECT_DIR":/doc \
        "$sphinx_docker_image" \
            make \
                SPHINXPORT=$sphinx_docker_expose_port \
                SPHINXIGNORE="$ignore_files" \
                livehtml \
    || true

    end_output_section
    print_line
}

function help() {
    echo -e ""
    echo -e "Run Sphinx in live HTML mode. Sphinx will watch the project directory and"
    echo -e "rebuild the documentation when a change is detected. The documentation will"
    echo -e "be available at http://localhost:<port>. The default port is 8000."
    echo -e ""
    echo -e "Usage: ./make-livehtml [OPTIONS]"
    echo -e "  -i, --interface INTERFACE"
    echo -e "                    The interface on the host machine Sphinx is bound to"
    echo -e "                    The default interface is $SPHINX_DOCKER_EXPOSE_INTERFACE"
    echo -e "  -n, --name NAME"
    echo -e "                    The name of the container"
    echo -e "                    The default name is $SPHINX_DOCKER_NAME"
    echo -e "  -p, --port PORT"
    echo -e "                    The port on the host machine Sphinx is bound to"
    echo -e "                    The default port is $SPHINX_DOCKER_EXPOSE_PORT"
    echo -e "  -h, --help"
    echo -e "                    Print this message and exit"
    echo -e ""
    echo -e "Examples:"
    echo -e "  ./make-livehtml"
    echo -e "  ./make-livehtml --name my-documentation --interface 0.0.0.0 --port 9000"
    echo -e ""
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    print_line
    print_failure_box "Building HTML documentation: Failed"
    print_line
    print_failure_highlight "An unexpected error has occured!"

    exit 1
}

trap error_handler ERR

main "$@"

exit 0
