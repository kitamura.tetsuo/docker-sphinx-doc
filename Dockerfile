#
# ddidier/sphinx-doc
#
# A Docker image for the Sphinx documentation builder (http://sphinx-doc.org).

FROM       python:3.8.6-slim-buster
MAINTAINER David DIDIER

# OpenJDK installation issue
# - https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=863199
# - mkdir -p /usr/share/man/man1

RUN mkdir -p /usr/share/man/man1 \
 && export DEBIAN_FRONTEND=noninteractive \
 && apt-get update \
 && apt-get install -y --no-install-recommends \
        gosu sudo \
        curl make \
        dvipng graphviz \
        openjdk-11-jre-headless \
        latexmk texlive-fonts-recommended texlive-latex-extra texlive-latex-recommended \
        texlive-lang-french \
        git \
    \
 && PLANTUML_VERSION=1.2020.19 \
 && mkdir /opt/plantuml \
 && curl -L https://sourceforge.net/projects/plantuml/files/${PLANTUML_VERSION}/plantuml.${PLANTUML_VERSION}.jar/download --output /opt/plantuml/plantuml.${PLANTUML_VERSION}.jar \
 && ln -s /opt/plantuml/plantuml.${PLANTUML_VERSION}.jar /opt/plantuml/plantuml.jar \
 && pip install --upgrade pip==20.2.3 \
 && pip install 'Sphinx                        == 3.2.1'    \
                'guzzle_sphinx_theme           == 0.7.11'   \
                'livereload                    == 2.6.3'    \
                'recommonmark                  == 0.6.0'    \
                'rinohtype                     == 0.4.0'    \
                'sphinx-autobuild              == 2020.9.1' \
                'sphinx_bootstrap_theme        == 0.7.1'    \
                'sphinx-copybutton             == 0.3.0'    \
                'sphinx-prompt                 == 1.3.0'    \
                'sphinx_rtd_theme              == 0.5.0'    \
                'sphinxcontrib-actdiag         == 2.0.0'    \
                'sphinxcontrib-blockdiag       == 2.0.0'    \
                'sphinxcontrib-excel-table     == 1.0.8'    \
                'sphinxcontrib-fulltoc         == 1.2.0'    \
                'sphinxcontrib-git-context     == 0.1.0'    \
                'sphinxcontrib-mermaid         == 0.5.0'    \
                'sphinxcontrib-nwdiag          == 2.0.0'    \
                'sphinxcontrib-plantuml        == 0.18.1'   \
                'sphinxcontrib-seqdiag         == 2.0.0'    \
    \
 && pip list --outdated   \
 && apt-get autoremove -y \
 && rm -rf /var/cache/* \
 && rm -rf /var/lib/apt/lists/*

ARG GIT_COMMIT_SHA
ARG GIT_COMMIT_TAG

COPY files/opt/ddidier/sphinx/bash/*        /opt/ddidier/sphinx/bash/
COPY files/opt/ddidier/sphinx/init/*        /opt/ddidier/sphinx/init/
COPY files/usr/local/bin/*                  /usr/local/bin/
COPY files/usr/share/ddidier/*              /usr/share/ddidier/

RUN chown root:root /usr/local/bin/* \
 && chmod 755 /usr/local/bin/*

ENV DATA_DIR=/doc \
    GIT_COMMIT_SHA=$GIT_COMMIT_SHA \
    GIT_COMMIT_TAG=$GIT_COMMIT_TAG

WORKDIR $DATA_DIR

ENTRYPOINT ["/usr/local/bin/docker-entrypoint"]
