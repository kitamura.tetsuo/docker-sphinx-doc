# NDD Docker Sphinx

[![pipeline status](https://gitlab.com/ddidier/docker-sphinx-doc/badges/master/pipeline.svg)](https://gitlab.com/ddidier/docker-sphinx-doc/commits/master)

<!-- MarkdownTOC -->

1. [Introduction](#introduction)
1. [Installation](#installation)
    1. [From source](#from-source)
    1. [From Docker Hub](#from-docker-hub)
1. [Usage](#usage)
    1. [Initialisation](#initialisation)
    1. [Non interactive mode](#non-interactive-mode)
        1. [Helper scripts](#helper-scripts)
        1. [Docker commands](#docker-commands)
    1. [Interactive mode](#interactive-mode)
    1. [Tips & Tricks](#tips--tricks)
1. [Configuration](#configuration)
    1. [Bundled extensions](#bundled-extensions)
    1. [Other extensions](#other-extensions)
1. [Limitations](#limitations)
1. [Development](#development)
    1. [Testing](#testing)
    1. [Testing locally with gitlab-runner](#testing-locally-with-gitlab-runner)
    1. [Releasing](#releasing)

<!-- /MarkdownTOC -->





<a id="introduction"></a>
## Introduction

[Sphinx documentation](http://sphinx-doc.org) builder with support for HTML, live HTML and PDF generation, and a lot more...

The versioning scheme of this Docker image is `<SPHINX_VERSION>-<DOCKER_IMAGE_VERSION>`.
For example, `1.8.1-9` stands for the 9th version of the Docker image using Sphinx 1.8.1.

Besides the [official Sphinx documentation builder](http://sphinx-doc.org),
this image contains and is configured for the following extensions:

- [alabaster](https://pypi.python.org/pypi/alabaster)
- [gitpython](https://pypi.python.org/pypi/gitpython)
- [guzzle-sphinx-theme](https://pypi.python.org/pypi/guzzle_sphinx_theme)
- [livereload](https://pypi.python.org/pypi/livereload)
- [recommonmark](https://pypi.org/project/recommonmark/)
- [rinohtype](https://pypi.org/project/rinohtype/)
- [sphinx-autobuild](https://pypi.org/project/sphinx-autobuild)
- [sphinx-bootstrap-theme](https://pypi.python.org/pypi/sphinx-bootstrap-theme)
- [sphinx-copybutton](https://pypi.org/project/sphinx-copybutton/)
- [sphinx-prompt](https://pypi.python.org/pypi/sphinx-prompt)
- [sphinx-rtd-theme](https://pypi.python.org/pypi/sphinx_rtd_theme)
- [sphinxcontrib-actdiag](https://pypi.python.org/pypi/sphinxcontrib-actdiag)
- [sphinxcontrib-blockdiag](https://pypi.python.org/pypi/sphinxcontrib-blockdiag)
- [sphinxcontrib-excel-table](https://pypi.python.org/pypi/sphinxcontrib-excel-table)
- [sphinxcontrib-fulltoc](https://pypi.org/project/sphinxcontrib-fulltoc)
- [sphinxcontrib-git_context](https://pypi.org/project/sphinxcontrib-git-context/)
- [sphinxcontrib-mermaid](https://pypi.org/project/sphinxcontrib-mermaid/)
- [sphinxcontrib-nwdiag](https://pypi.python.org/pypi/sphinxcontrib-nwdiag)
- [sphinxcontrib-plantuml](https://pypi.python.org/pypi/sphinxcontrib-plantuml)
- [sphinxcontrib-seqdiag](https://pypi.python.org/pypi/sphinxcontrib-seqdiag)

This image also contains some utility scripts to make your life easier.

References:

- image on Docker Hub : https://hub.docker.com/r/ddidier/sphinx-doc
- sources on GitLab : https://gitlab.com/ddidier/docker-sphinx-doc

The image is based on the official [python:3.8](https://hub.docker.com/_/python/).





<a id="installation"></a>
## Installation

<a id="from-source"></a>
### From source

```shell
git clone git@gitlab.com:ddidier/docker-sphinx-doc.git
cd docker-sphinx-doc
./bin/build-and-test.sh
```

<a id="from-docker-hub"></a>
### From Docker Hub

```shell
export SPHINX_DOC_VERSION=...
docker pull ddidier/sphinx-doc:$SPHINX_DOC_VERSION
```





<a id="usage"></a>
## Usage

The documentation directory on the host is called `<HOST_DATA_DIR>` in the remainder of this document.

The directory `<HOST_DATA_DIR>` on the host must be mounted as a volume under `/doc` in the container.
Use `-v <HOST_DATA_DIR>:/doc` to use a specific documentation directory or `-v $(pwd):/doc` to use the current directory as the documentation directory.

Sphinx will be executed inside the container by the `sphinx-doc` user which is created by the Docker entry point.
You **must** pass to the container the environment variable `USER_ID` set to the UID of the user the files will belong to.
This is the ``-e USER_ID=$UID `` part in the examples of this documentation.

**Except for the initialisation phase, custom scripts are provided which already take care of this plumbing.**



<a id="initialisation"></a>
### Initialisation

Sphinx provides the [`sphinx-quickstart`](https://www.sphinx-doc.org/en/master/man/sphinx-quickstart.html) script to create a skeleton of the documentation directory.
You should however use the custom tailored `sphinx-init` script which customize the generated configuration files and add the utility scripts.

**The directory `<HOST_DATA_DIR>` must already exist, otherwise the script will fail!**

```shell
docker run -it -v <HOST_DATA_DIR>:/doc -e USER_ID=$UID ddidier/sphinx-doc:$SPHINX_DOC_VERSION sphinx-init
```

All arguments accepted by [`sphinx-quickstart`](http://sphinx-doc.org/invocation.html) are passed to `sphinx-init`.
For example, you may want to pass the project name on the command line:

```shell
docker run -it -v <HOST_DATA_DIR>:/doc -e USER_ID=$UID ddidier/sphinx-doc:$SPHINX_DOC_VERSION sphinx-init --project my-documentation
```



<a id="non-interactive-mode"></a>
### Non interactive mode

The so-called *non-interactive mode* is when you issue commands from the host directly.

<a id="helper-scripts"></a>
#### Helper scripts

**Helper scripts are provided to help you with common tasks and are the recommanded way to go.**
They are located in the `bin` directory of the generated project.
They are configured with default variables located in the `bin/variables.sh` file.
Each one of them can be called with `--help` to display all the available options.

To generate the HTML documentation, call:

```shell
./bin/make-html
```

To generate the HTML documentation, and watch for changes with live reload, call:

```shell
# use the default port (i.e. 8000)
./bin/make-livehtml

# use a custom port (e.g. 12345) so you can have multiple builds at the same time
./bin/make-livehtml --port 12345 livehtml

# use --help to see all the available options
./bin/make-livehtml --help

Run Sphinx in live HTML mode. Sphinx will watch the project directory and
rebuild the documentation when a change is detected. The documentation will
be available at http://localhost:<port>. The default port is 8000.

Usage: ./make-livehtml [OPTIONS]
  -i, --interface INTERFACE
                    The interface on the host machine Sphinx is bound to
                    The default interface is 127.0.0.1
  -n, --name NAME
                    The name of the container
                    The default name is tmp.qB51vcL0In.docker-sphinx-doc
  -p, --port PORT
                    The port on the host machine Sphinx is bound to
                    The default port is 8000
  -h, --help
                    Print this message and exit

Examples:
  ./make-livehtml
  ./make-livehtml --name my-documentation --interface 0.0.0.0 --port 9000


```

To generate the PDF documentation, call:

```shell
./bin/make-pdf
```

<a id="docker-commands"></a>
#### Docker commands

You can of course directly use the sphinx commands without relying on the helper scripts.

To see all the official targets, call:

```shell
docker run -i -v <HOST_DATA_DIR>:/doc -e USER_ID=$UID ddidier/sphinx-doc:$SPHINX_DOC_VERSION make help
```

To generate the HTML documentation, call `make html`:

```shell
docker run -i -v <HOST_DATA_DIR>:/doc -e USER_ID=$UID ddidier/sphinx-doc:$SPHINX_DOC_VERSION make html
```

To generate the HTML documentation, and watch for changes with live reload, call `make livehtml`:

```shell
# use the default port (i.e. 8000)
docker run -it -v <HOST_DATA_DIR>:/doc -p 8000:8000 -e USER_ID=$UID ddidier/sphinx-doc:$SPHINX_DOC_VERSION make livehtml
#                                         ^^^^
#                                         open your browser at http://localhost:8000/

# use a custom port (e.g. 12345) so you can have multiple builds at the same time
docker run -it -v <HOST_DATA_DIR>:/doc -p 12345:12345 -e USER_ID=$UID ddidier/sphinx-doc make:$SPHINX_DOC_VERSION SPHINXPORT=12345 livehtml
#                                         ^^^^^                                                                   ^^^^^^^^^^^^^^^^
#                                         open your browser at http://localhost:12345/                            customize server port
```

To trigger a full build while in watch mode, issue from the `<HOST_DATA_DIR>` folder on the host:

```shell
rm -rf build && touch source/conf.py
```

To generate the PDF documentation, call `make latexpdf`:

```shell
docker run -i -v <HOST_DATA_DIR>:/doc -e USER_ID=$UID ddidier/sphinx-doc:$SPHINX_DOC_VERSION make latexpdf
```



<a id="interactive-mode"></a>
### Interactive mode

The so-called *interactive mode* is when you issue commands from inside the container.

```shell
docker run -it -v <HOST_DATA_DIR>:/doc -e USER_ID=$UID ddidier/sphinx-doc:$SPHINX_DOC_VERSION
```

You should now be in the `/doc` directory, otherwise just `cd` to `/doc`.

To see all the official targets, call `make help`.

To generate the HTML documentation, call `make html`.

To generate the PDF documentation, call `make latexpdf`.

<a id="tips--tricks"></a>
### Tips & Tricks

If you need the directory `<HOST_DATA_DIR>` to NOT be the root of the documentation, change the `make` directory with `-C`.
Previous commands such as `make target` become `make -C /some/directory/ target`.
Please see the pseudo [Git extension below](#git-extension) for an example.



<a id="configuration"></a>
## Configuration

**Warning: to simplify the `sphinx-init` script, some variables are overriden at the end of the `conf.py` file.**
They appear twice in the file, so be sure to update the last one if needed.
This is most notably the case of the `extensions` and `html_theme` variables.

<a id="bundled-extensions"></a>
### Bundled extensions

This image comes with a number of already bundled extensions.

To enable a bundled extension, simply uncomment the associated line in your `conf.py`.

To disable a bundled extension, simply comment the associated line in your `conf.py`.

<a id="other-extensions"></a>
### Other extensions

If you want to use an extension which is not already bundled with this image, you need to:

1. create a new `Dockerfile` extending the `ddidier/sphinx-doc` image
2. install the Python package(s) of the extension(s) inside the `Dockerfile`
3. reference the extension(s) in the Sphinx configuration file

```docker
#
# Dockerfile
#
FROM ddidier/sphinx-doc:latest

RUN pip install 'a-sphinx-extension       == A.B.C' \
                'another-sphinx-extension == X.Y.Z'
```

```python
#
# source/conf.py
#
extensions = [
    ...
    'a.sphinx.extension',
    'another.sphinx.extension',
]
```



<a id="limitations"></a>
## Limitations

- PDF generation does not take into account Excel tables.



<a id="development"></a>
## Development

<a id="testing"></a>
### Testing

The script `./bin/build-and-test.sh`, as the name implies, builds the image then runs all the tests.

If you want to run only some of the tests, you can use:

- `./tests/test-init/test.sh` to test project initialisation
- `./tests/test-build/test.sh --html` to test HTML documentation generation
- `./tests/test-build/test.sh --pdf` to test PDF documentation generation
- `./tests/test.sh` combines all the previous tests

**Do not forget to rebuild the Docker image beforehand! `./bin/build.sh`.**


<a id="testing-locally-with-gitlab-runner"></a>
### Testing locally with gitlab-runner

GitLab-CI is used to do continuous integration testing.

The GitLab-CI environment has some differences with your local environment, mostly:

- the build is done using Docker in Docker
- the Docker image is based on Alpine

The GitLab-CI pipeline takes some time to complete so tests should be done locally before pushing modifications...

1. Start the GitLab Runner service:

    ```shell
    export GITLAB_RUNNER_VERSION=v12.9.0

    docker run -d \
        --name gitlab-runner \
        --privileged \
        -v /srv/gitlab-runner/config:/etc/gitlab-runner \
        -v /var/run/docker.sock:/var/run/docker.sock \
        gitlab/gitlab-runner:$GITLAB_RUNNER_VERSION
    ```

2. Comment out the Docker image pull in `gitlab-ci.yml` since the test will use your local image:

    ```yaml
    Test:
      stage: test
      script:
        # Comment this out when testing locally with gitlab-runner
        # ----------
        # - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
        # - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $DOCKERHUB_IMAGE_NAME:latest
        # ----------
        - export TMPDIR=$CI_PROJECT_DIR
        - $CI_PROJECT_DIR/tests/test.sh
    ```

3. Change what you need
4. Ensure that all modifications are committed, e.g.

    ```shell
    git add --all
    git commit -m "Test or whatever"
    ```

5. Ensure that all modifications have found their way to the Docker image:

    ```shell
    ./bin/build.sh
    ```

6. Run the test:

    ```shell
    docker run --rm -t -i \
        -v /opt/gitlab-runner/config:/etc/gitlab-runner \
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v $PWD:$PWD \
        --workdir $PWD \
        gitlab/gitlab-runner:alpine-$GITLAB_RUNNER_VERSION \
            exec docker \
                --docker-privileged \
                --docker-pull-policy="if-not-present" \
                --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
                --docker-volumes /builds:/builds \
                Test
    ```

7. Repeat from step #3 as needed

Once you've done:

- delete the build directory with `rm -rf /builds`
- revert the comment you've done in `gitlab-ci.yml`

<a id="releasing"></a>
### Releasing

Note to myself...

Using Gitflow:

1. Start a new release branch named `<SPHINX_VERSION>-<DOCKER_IMAGE_VERSION>`
2. Update the changelog
3. Build the Docker image with `./bin/build.sh`
4. Run the tests with `./bin/test.sh`
6. Create a Git commit named `Update CHANGELOG for version <SPHINX_VERSION>-<DOCKER_IMAGE_VERSION>`
7. Push the release branch to the remote origin
8. Wait for the remote build to finish
9. End the release branch
10. Tag the merge commit on the master branch
11. Don't forget to manually update the README on Docker Hub
