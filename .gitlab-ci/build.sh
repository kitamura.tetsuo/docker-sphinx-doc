#!/bin/bash

set -e

#
# Build the Docker image and push it to the GitLab registry.
#
# Parameters:
#
# $CI_COMMIT_SHA
# $CI_COMMIT_TAG
# $CI_REGISTRY
# $CI_REGISTRY_IMAGE
# $CI_REGISTRY_PASSWORD
# $CI_REGISTRY_USER
#
# @version 0.2.0
# @author David DIDIER
# @see https://gitlab.com/ddidier/docker-tools
#

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# shellcheck source=.gitlab-ci/commons.sh
source "$SCRIPT_DIR/commons.sh"

print_h1 "Build the Docker image and push it to the GitLab registry"

print_h2 "Script parameters"
echo "CI_COMMIT_SHA = $CI_COMMIT_SHA"
echo "CI_COMMIT_TAG = $CI_COMMIT_TAG"
echo "CI_REGISTRY = $CI_REGISTRY"
echo "CI_REGISTRY_IMAGE = $CI_REGISTRY_IMAGE"
echo "CI_REGISTRY_PASSWORD = **********"
echo "CI_REGISTRY_USER = $CI_REGISTRY_USER"

print_h2 "Current Docker informations"
docker info

print_h2 "Authenticating against the GitLab registry"
docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"

if [ "$CI_COMMIT_TAG" == "" ];then
    CI_COMMIT_TAG="tested"
fi

print_h2 "Building and tagging the image"
docker build \
    --tag "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA" \
    --build-arg GIT_COMMIT_SHA="$CI_COMMIT_SHA" \
    --build-arg GIT_COMMIT_TAG="$CI_COMMIT_TAG" \
    .

print_h2 "Pushing the tagged image to the GitLab registry"
docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA"
