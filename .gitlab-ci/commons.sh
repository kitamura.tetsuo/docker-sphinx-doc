#!/bin/bash

#
# Utilities to include in scripts.
#
# @version 0.2.0
# @author David DIDIER
# @see https://gitlab.com/ddidier/docker-tools
#

export TXT_BLACK='\e[0;30m'
export TXT_RED='\e[0;31m'
export TXT_GREEN='\e[0;32m'
export TXT_YELLOW='\e[0;33m'
export TXT_BLUE='\e[0;34m'
export TXT_PURPLE='\e[0;35m'
export TXT_CYAN='\e[0;36m'
export TXT_WHITE='\e[0;37m'

export BOLD_BLACK='\e[1;30m'
export BOLD_RED='\e[1;31m'
export BOLD_GREEN='\e[1;32m'
export BOLD_YELLOW='\e[1;33m'
export BOLD_BLUE='\e[1;34m'
export BOLD_PURPLE='\e[1;35m'
export BOLD_CYAN='\e[1;36m'
export BOLD_WHITE='\e[1;37m'

export UND_BLACK='\e[4;30m'
export UND_RED='\e[4;31m'
export UND_GREEN='\e[4;32m'
export UND_YELLOW='\e[4;33m'
export UND_BLUE='\e[4;34m'
export UND_PURPLE='\e[4;35m'
export UND_CYAN='\e[4;36m'
export UND_WHITE='\e[4;37m'

export BG_BLACK='\e[40m'
export BG_RED='\e[41m'
export BG_GREEN='\e[42m'
export BG_YELLOW='\e[43m'
export BG_BLUE='\e[44m'
export BG_PURPLE='\e[45m'
export BG_CYAN='\e[46m'
export BG_WHITE='\e[47m'

export TXT_RESET='\e[0m'

export SYMBOL_CHECK_MARK="✔️"
export SYMBOL_COLLISION="💥"
export SYMBOL_CROSS_MARK="❌"
export SYMBOL_EXCLAMATION_MARK="❗"
export SYMBOL_FIRE="🔥"
export SYMBOL_NO_ENTRY="⛔"
export SYMBOL_RADIOACTIVE="☢️"
export SYMBOL_WARNING="⚠️"

#
# Print a title at level 1.
#
# $1 - the title to print.
#
function print_h1() {
    declare -r title="${1}"
    printf "${BOLD_YELLOW}%s${TXT_RESET}\n" "${title}"
}

#
# Print a title at level 2.
#
# $1 - the title to print.
#
function print_h2() {
    declare -r title="${1}"
    printf "${UND_YELLOW}%s${TXT_RESET}\n" "${title}"
}

#
# Print a warning message.
#
# $1 - the message to print.
#
function print_warning() {
    declare -r message="${1}"
    printf "${BOLD_YELLOW}${SYMBOL_WARNING}  WARNING: %s${TXT_RESET}\n" "${message}"
}

#
# Print an error message.
#
# $1 - the message to print.
#
function print_error() {
    declare -r message="${1}"
    printf "${BOLD_RED}${SYMBOL_CROSS_MARK} ERROR: %s${TXT_RESET}\n" "${message}"
}

#
# Print an error message and exit.
#
# $1 - the message to print.
#
function print_fatal() {
    declare -r message="${1}"
    printf "${BOLD_RED}${SYMBOL_COLLISION} FATAL: %s${TXT_RESET}\n" "${message}"
    exit 1
}

#
# Check that the parameter is a valid semantic version, i.e.
#
# $1 - the parameter to check.
#
function check_semantic_version() {
    declare -r tag="${1}"
    if [[ ! "${tag}" =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
        print_fatal "Tag '${tag}' is not a semantic version"
    fi
}

#
# Get a Docker Hub JWT token.
#
# $1 - the Docker Hub username.
# $2 - the Docker Hub password.
#
# @return the Docker Hub JWT token.
#
function get_token() {
    declare -r username="${1}"
    declare -r password="${2}"

    curl -s \
         -H "Content-Type: application/json" \
         -X POST \
         -d '{ "username": "'${username}'", "password": "'${password}'" }' \
         "https://hub.docker.com/v2/users/login/" \
    | jq -r .token
}

#
# Pushes README.md content to Docker Hub.
#
# $1 - The image name.
# $2 - The JWT token.
#
function push_readme() {
    declare -r image="${1}"
    declare -r token="${2}"

    local code=
    code=$( \
        jq \
            -n --arg msg "$(<README.md)" \
            '{"registry": "registry-1.docker.io", "full_description": $msg }' \
        | curl \
            -s -o /dev/null  -L -w "%{http_code}" \
            https://cloud.docker.com/v2/repositories/"${image}"/ \
            -d @- -X PATCH \
            -H "Content-Type: application/json" \
            -H "Authorization: JWT ${token}" \
    )

    if [[ "${code}" = "200" ]]; then
        printf "Pushed README to Docker Hub\n"
    else
        print_fatal "Unable to push README to Docker Hub, response code: ${code}"
    fi
}
