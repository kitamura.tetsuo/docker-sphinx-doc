#!/bin/bash

#
# Utilities to include in scripts.
#
# @version 0.1.0
# @author David DIDIER
# @see TODO
#

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

export TXT_BLACK='\e[0;30m'
export TXT_RED='\e[0;31m'
export TXT_GREEN='\e[0;32m'
export TXT_YELLOW='\e[0;33m'
export TXT_BLUE='\e[0;34m'
export TXT_PURPLE='\e[0;35m'
export TXT_CYAN='\e[0;36m'
export TXT_WHITE='\e[0;37m'

export BOLD_BLACK='\e[1;30m'
export BOLD_RED='\e[1;31m'
export BOLD_GREEN='\e[1;32m'
export BOLD_YELLOW='\e[1;33m'
export BOLD_BLUE='\e[1;34m'
export BOLD_PURPLE='\e[1;35m'
export BOLD_CYAN='\e[1;36m'
export BOLD_WHITE='\e[1;37m'

export UND_BLACK='\e[4;30m'
export UND_RED='\e[4;31m'
export UND_GREEN='\e[4;32m'
export UND_YELLOW='\e[4;33m'
export UND_BLUE='\e[4;34m'
export UND_PURPLE='\e[4;35m'
export UND_CYAN='\e[4;36m'
export UND_WHITE='\e[4;37m'

export BG_BLACK='\e[40m'
export BG_RED='\e[41m'
export BG_GREEN='\e[42m'
export BG_YELLOW='\e[43m'
export BG_BLUE='\e[44m'
export BG_PURPLE='\e[45m'
export BG_CYAN='\e[46m'
export BG_WHITE='\e[47m'

export TXT_RESET='\e[0m'

export SYMBOL_CHECK_MARK="✔️"
export SYMBOL_COLLISION="💥"
export SYMBOL_CROSS_MARK="❌"
export SYMBOL_EXCLAMATION_MARK="❗"
export SYMBOL_FIRE="🔥"
export SYMBOL_INFORMATION="ℹ️"
export SYMBOL_NO_ENTRY="⛔"
export SYMBOL_RADIOACTIVE="☢️"
export SYMBOL_WARNING="⚠️"



#
# Print a message on a new line.
#
# $1 - the message to print.
#
function print_line() {
    declare -r message="${1:-}"
    echo -e "$message"
}

#
# Print a message on a new line at success level.
#
# $1 - the message to print.
#
function print_line_success() {
    declare -r message="${1:-}"
    echo -e "${BOLD_GREEN}${message}${TXT_RESET}"
}

#
# Print a message on a new line at failure level.
#
# $1 - the message to print.
#
function print_line_failure() {
    declare -r message="${1:-}"
    echo -e "${BOLD_RED}${message}${TXT_RESET}"
}

#
# Print a message on a new line at information level.
#
# $1 - the message to print.
#
function print_line_information() {
    declare -r message="${1:-}"
    echo -e "${BOLD_BLUE}${message}${TXT_RESET}"
}



# -------------------------------------------------------------------------------------------------------- headers -----

#
# Print a title at level 1.
#
# $1 - the title to print.
#
function print_h1() {
    declare -r title="${1}"
    echo -e -n "$BOLD_YELLOW"
    echo -e " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    echo -e " ┃ $title"
    echo -e " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    echo -e -n "$TXT_RESET"
}

#
# Print a highlighted text at level 1.
#
# $1 - the text to print.
#
function print_h1_highlight() {
    declare -r text="${1}"
    echo -e "$BOLD_YELLOW ┃ ${text}$TXT_RESET"
}



#
# Print a title at level 2.
#
# $1 - the title to print.
#
function print_h2() {
    declare -r title="${1}"
    echo -e -n "$TXT_YELLOW"
    echo -e " ┌──────────────────────────────────────────────────────────────────────────────"
    echo -e " │ $title"
    echo -e " └──────────────────────────────────────────────────────────────────────────────"
    echo -e -n "$TXT_RESET"
}

#
# Print a highlighted text at level 2.
#
# $1 - the text to print.
#
function print_h2_highlight() {
    declare -r text="${1}"
    echo -e "$TXT_YELLOW ┃ ${text}$TXT_RESET"
}



# -------------------------------------------------------------------------------------------------------- success -----

#
# Print a success message on a single line.
#
# $1 - the message to print.
#
function print_success() {
    declare -r message="${1}"
    echo -e "$BOLD_GREEN $SYMBOL_CHECK_MARK  ${message}$TXT_RESET"
}

#
# Print a success message within a box.
#
# $1 - the message to print.
#
function print_success_box() {
    declare -r message="${1}"
    echo -e -n "$BOLD_GREEN"
    echo -e " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    echo -e " ┃ $SYMBOL_CHECK_MARK  $message"
    echo -e " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    echo -e -n "$TXT_RESET"
}

#
# Print a highlighted text for success.
#
# $1 - the text to print.
#
function print_success_highlight() {
    declare -r text="${1}"
    echo -e "$BOLD_GREEN ┃ ${text}$TXT_RESET"
}



# -------------------------------------------------------------------------------------------------------- failure -----

#
# Print a failure message on a single line.
#
# $1 - the message to print.
#
function print_failure() {
    declare -r message="${1}"
    echo -e "$BOLD_RED $SYMBOL_CROSS_MARK  ${message}$TXT_RESET"
}

#
# Print a failure message within a box.
#
# $1 - the message to print.
#
function print_failure_box() {
    declare -r message="${1}"
    echo -e -n "$BOLD_RED"
    echo -e " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    echo -e " ┃ $SYMBOL_CROSS_MARK  $message"
    echo -e " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    echo -e -n "$TXT_RESET"
}

#
# Print a highlighted text for failure.
#
# $1 - the text to print.
#
function print_failure_highlight() {
    declare -r text="${1}"
    echo -e "$BOLD_RED ┃ ${text}$TXT_RESET"
}



# ---------------------------------------------------------------------------------------------------- information -----

#
# Print an information message on a single line.
#
# $1 - the message to print.
#
function print_information() {
    declare -r message="${1}"
    echo -e "$BOLD_BLUE $SYMBOL_INFORMATION  ${message}$TXT_RESET"
}

#
# Print an information message within a box.
#
# $1 - the message to print.
#
function print_information_box() {
    declare -r message="${1}"
    echo -e -n "$BOLD_BLUE"
    echo -e " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    echo -e " ┃ $SYMBOL_INFORMATION  $message"
    echo -e " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    echo -e -n "$TXT_RESET"
}

#
# Print a highlighted text for information.
#
# $1 - the text to print.
#
function print_information_highlight() {
    declare -r text="${1}"
    echo -e "$BOLD_BLUE ┃ ${text}$TXT_RESET"
}



# --------------------------------------------------------------------------------------------------------- output -----

#
# Print a delimiter starting the output of another script.
function start_output_section() {
    echo -e "$BOLD_BLUE ▼ ~~~~~ start of output ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~$TXT_RESET"
}

#
# Print a delimiter ending the output of another script.
#
function end_output_section() {
    echo -e "$BOLD_BLUE ▲ ~~~~~ end of output ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~$TXT_RESET"
}



# -------------------------------------------------------------------------------------------------- miscellaneous -----

#
# Print a step definition.
#
# $1 - the message to print.
#
function print_step() {
    declare -r message="${1}"
    echo -e " ⮕  $message"
}

#
# Print a step definition at success level.
#
# $1 - the message to print.
#
function print_step_success() {
    declare -r message="${1}"
    echo -e "$BOLD_GREEN ⮕  ${message}$TXT_RESET"
}

#
# Print a step definition at failure level.
#
# $1 - the message to print.
#
function print_step_failure() {
    declare -r message="${1}"
    echo -e "$BOLD_RED ⮕  ${message}$TXT_RESET"
}

#
# Print a step definition at information level.
#
# $1 - the message to print.
#
function print_step_information() {
    declare -r message="${1}"
    echo -e "$BOLD_BLUE ⮕  ${message}$TXT_RESET"
}



# --------------------------------------------------------------------------------------------------------- status -----

#
# Print a warning message.
#
# $1 - the message to print.
#
function print_warning() {
    declare -r message="${1}"
    echo -e "$BOLD_YELLOW $SYMBOL_WARNING  WARNING: ${message}$TXT_RESET"
}

#
# Print an error message.
#
# $1 - the message to print.
#
function print_error() {
    declare -r message="${1}"
    echo -e "$BOLD_RED $SYMBOL_CROSS_MARK ERROR: ${message}$TXT_RESET"
}

#
# Print an error message.
#
# $1 - the message to print.
#
function print_fatal() {
    declare -r message="${1}"
    echo -e "$BOLD_RED $SYMBOL_COLLISION FATAL: ${message}$TXT_RESET"
}



# ----------------------------------------------------------------------------------------------- string utilities -----

function join_by {
    local IFS="$1"
    shift
    echo "$*"
}





# ====================================================================================================== auto test =====
#
# TEST_COMMONS=1 bash /home/ddidier/Projets/Personnel/Docker/docker-sphinx-doc/bin/commons.sh
#
TEST_COMMONS=${TEST_COMMONS:-0}

if [[ $TEST_COMMONS -eq 1 ]]; then

function __________________________________________________separator() {
    echo "============================== $1"
}

echo
echo
echo
echo
echo

__________________________________________________separator "print_line without argument"
print_line
__________________________________________________separator "print_line with argument"
print_line "Some message"
__________________________________________________separator "print_line_success"
print_line_success "Some message"
__________________________________________________separator "print_line_failure"
print_line_failure "Some message"
__________________________________________________separator "print_line_information"
print_line_information "Some message"

__________________________________________________separator "print_h1"
print_h1 "Title Level 1"
__________________________________________________separator "print_h1_highlight"
print_h1_highlight "Text Level 1"

__________________________________________________separator "print_h2"
print_h2 "Title Level 2"
__________________________________________________separator "print_h2_highlight"
print_h2_highlight "Text Level 2"

__________________________________________________separator "print_success"
print_success "A beautiful success!"
__________________________________________________separator "print_success_box"
print_success_box "A beautiful success!"
__________________________________________________separator "print_success_highlight"
print_success_highlight "A beautiful success!"

__________________________________________________separator "print_failure"
print_failure "A horrible failure!"
__________________________________________________separator "print_failure_box"
print_failure_box "A horrible failure!"
__________________________________________________separator "print_failure_highlight"
print_failure_highlight "A horrible failure!"

__________________________________________________separator "print_information"
print_information "A useful information"
__________________________________________________separator "print_information_box"
print_information_box "A useful information"
__________________________________________________separator "print_information_highlight"
print_information_highlight "A useful information"

__________________________________________________separator "start_output_section"
start_output_section
__________________________________________________separator "end_output_section"
end_output_section

__________________________________________________separator "print_step"
print_step "A hundred steps"
__________________________________________________separator "print_step_success"
print_step_success "A hundred steps"
__________________________________________________separator "print_step_failure"
print_step_failure "A hundred steps"
__________________________________________________separator "print_step_information"
print_step_information "A hundred steps"

__________________________________________________separator "print_warning"
print_warning "Some message"
__________________________________________________separator "print_error"
print_error "Some message"
__________________________________________________separator "print_fatal"
print_fatal "Some message"

__________________________________________________separator "join_by , a \"b c\" d"
join_by , a "b c" d

echo
echo
echo
echo
echo

fi
