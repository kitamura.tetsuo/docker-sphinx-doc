#!/bin/bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_DIR="$(realpath "$SCRIPT_DIR/..")"

# shellcheck source=/dev/null
source "$SCRIPT_DIR/commons.sh"

function main() {

    print_line
    print_h1 "Building Docker image 'ddidier/sphinx-doc'"

    local git_commit_sha
    local git_commit_tag
    git_commit_sha=$(git log -1 --pretty=%H)
    git_commit_tag=$(git describe --exact-match --tags "$git_commit_sha" 2> /dev/null || echo "tested")

    print_line
    print_h1_highlight "Using Git commit:"
    print_h1_highlight "- SHA = $git_commit_sha"
    print_h1_highlight "- TAG = $git_commit_tag"

    print_line
    start_output_section
    docker build \
        --tag "ddidier/sphinx-doc" \
        --build-arg GIT_COMMIT_SHA="$git_commit_sha" \
        --build-arg GIT_COMMIT_TAG="$git_commit_tag" \
        "$PROJECT_DIR"
    end_output_section

    print_line
    print_success_box "Docker image 'ddidier/sphinx-doc' built"
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    print_line
    print_failure_box "Docker image creation: Failed"
    print_line
    print_failure_highlight  "An unexpected error has occured!"

    exit 1
}

trap error_handler ERR

main "$@"

exit 0
