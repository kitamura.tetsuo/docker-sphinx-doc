#!/bin/bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TESTS_DIR="$(realpath "$SCRIPT_DIR/..")"
PROJECT_DIR="$(realpath "$TESTS_DIR/..")"

# shellcheck source=/dev/null
source "$PROJECT_DIR/bin/commons.sh"

function main() {

    print_line
    print_h1 "Testing initialization of a Sphinx project"

    local temp_dir
    local temp_dir_link
    temp_dir=$(mktemp -d --suffix=.docker-sphinx-doc)
    temp_dir_link="$(dirname "$temp_dir")/docker-sphinx-doc"

    rm -rf "$temp_dir_link"
    ln -sf "$temp_dir" "$temp_dir_link"

    print_line
    print_h1_highlight "Data will be stored in the temporary directory:"
    print_h1_highlight "  $temp_dir"
    print_h1_highlight "Use the following link for convenience:"
    print_h1_highlight "  $temp_dir_link"

    print_line
    print_step "Getting Docker image metadata"

    local docker_image_sha
    local docker_image_tag
    docker_image_sha=$(docker run -e USER_ID=$UID "ddidier/sphinx-doc:latest" bash -c 'echo "$GIT_COMMIT_SHA"')
    docker_image_tag=$(docker run -e USER_ID=$UID "ddidier/sphinx-doc:latest" bash -c 'echo "$GIT_COMMIT_TAG"')
    print_line
    print_line "  - Docker image hash = $docker_image_sha"
    print_line "  - Docker image tag  = $docker_image_tag"

    print_line
    print_step "Tagging image 'ddidier/sphinx-doc:latest' with 'ddidier/sphinx-doc:tested'"
    docker tag ddidier/sphinx-doc:latest ddidier/sphinx-doc:tested

    print_line
    print_step "Generating project using 'docker run ... ddidier/sphinx-doc:latest sphinx-init ...'"
    print_line
    start_output_section
    docker run --rm -v "$temp_dir":/doc -e USER_ID=$UID ddidier/sphinx-doc:latest \
        sphinx-init --project=MYPROJECT --author=MYSELF --sep --quiet
    end_output_section

    print_line
    print_step "Checking Docker image version in generated files"

    if ! grep "SPHINX_DOCKER_IMAGE_VERSION=\"$docker_image_tag\"" "$temp_dir/bin/variables.sh" > /dev/null; then
        print_line
        print_failure_box "Initialization Test: Failed"
        print_line
        print_failure_highlight "Wrong or missing commit tag in '$temp_dir_link/bin/variables.sh'"
        print_failure_highlight "  Expected: 'SPHINX_DOCKER_IMAGE_VERSION=\"$docker_image_tag\"'"
        exit 1
    fi

    print_line
    print_step "Replacing Docker image version in variables"
    sed -i -E "s/(SPHINX_DOCKER_IMAGE_VERSION=)\".+\"/\1\"tested\"/g" "$temp_dir/bin/variables.sh"

    if [ "$docker_image_tag" != "tested" ]; then
        git --git-dir="$temp_dir/.git" config user.email "test@example.com"
        git --git-dir="$temp_dir/.git" config user.name "Test"
        git --git-dir="$temp_dir/.git" --work-tree="$temp_dir" add --all > /dev/null
        git --git-dir="$temp_dir/.git" --work-tree="$temp_dir" commit -m "Change Docker image version" > /dev/null
    fi

    print_line
    print_step "Generating HTML documentation using 'make-html'"
    print_line
    start_output_section
    "$temp_dir/bin/make-html"
    end_output_section

    print_line
    print_step "Checking the generated Git commits"

    local actual_git_log_messages
    local expected_git_log_messages
    actual_git_log_messages="$(git --git-dir="$temp_dir/.git" log --pretty=%s | tr '\n' '@')"

    if [ "$docker_image_tag" != "tested" ]; then
        expected_git_log_messages="Change Docker image version@Generate skeleton@Initial commit@"
    else
        expected_git_log_messages="Generate skeleton@Initial commit@"
    fi

    if [ "$actual_git_log_messages" != "$expected_git_log_messages" ];then
        print_line
        print_failure_box "Initialization Test: Failed"
        print_line
        print_failure_highlight "Wrong or missing generated commit"
        print_failure_highlight "  Expected: '$expected_git_log_messages'"
        print_failure_highlight "  Actual:   '$actual_git_log_messages'"
        exit 1
    fi

    print_line
    print_step "Checking the commit tag and the commit hash in the generated files"

    if ! grep "ddidier_sphinxdoc_git_sha = '$docker_image_sha'" "$temp_dir/source/conf.py" > /dev/null; then
        print_line
        print_failure_box "Initialization Test: Failed"
        print_line
        print_failure_highlight "Wrong or missing commit hash in '$temp_dir_link/source/conf.py'"
        print_failure_highlight "  Expected: 'ddidier_sphinxdoc_git_sha = \'$docker_image_sha\''"
        exit 1
    fi

    if ! grep "ddidier_sphinxdoc_git_tag = '$docker_image_tag'" "$temp_dir/source/conf.py" > /dev/null; then
        print_line
        print_failure_box "Initialization Test: Failed"
        print_line
        print_failure_highlight "Wrong or missing commit tag in '$temp_dir_link/source/conf.py'"
        print_failure_highlight "  Expected: 'ddidier_sphinxdoc_git_tag = \'$docker_image_tag\''"
        exit 1
    fi

    sed -i -E "s/(ddidier_sphinxdoc_git_sha =) '.+'/\1 'TESTED_IN_SCRIPT'/g" "$temp_dir/source/conf.py"
    sed -i -E "s/(ddidier_sphinxdoc_git_tag =) '.*'/\1 'TESTED_IN_SCRIPT'/g" "$temp_dir/source/conf.py"

    print_line
    print_step "Getting rid of the generation date"
    sed -i -E 's/(sphinx-quickstart on) .+/\1 DATE_REMOVED./g' "$temp_dir/source/index.rst"
    sed -i -E 's/(sphinx-quickstart on) .+/\1 DATE_REMOVED./g' "$temp_dir/build/html/_sources/index.rst.txt"

    print_line
    print_step "Getting rid of the Git metadata"
    find "$temp_dir/build" -type f -exec \
        sed -i -E 's/Commit: [0-9a-f]{40}/Commit: FULL_COMMIT_HASH/g' {} \;
    find "$temp_dir/build" -type f -exec \
        sed -i -E 's/Commit: [0-9a-f]{8}/Commit: SHORT_COMMIT_HASH/g' {} \;
    find "$temp_dir/build" -type f -exec \
        sed -i -E 's/Last updated on [[:digit:]]{4}\/[[:digit:]]{2}\/[[:digit:]]{2} [[:digit:]]{2}:[[:digit:]]{2}:[[:digit:]]{2}/Last updated on LAST_UPDATED/g' {} \;
    find "$temp_dir/build" -type f -exec \
        sed -i -E 's/config: [0-9a-f]{32}/config: CONFIG_HASH/g' {} \;

    print_line
    print_step "Renaming .gitignore"

    mv "$temp_dir/.gitignore" "$temp_dir/.gitignore-to-rename-before-comparison"

    print_line
    print_step "Computing differences between actual and expected files"

    local expected_dir="$SCRIPT_DIR/expected"

    local delta
    delta=$(diff -r --exclude ".git" --exclude "doctrees" --exclude "__pycache__" "$temp_dir" "$expected_dir" || true)

    if [ "$delta" != "" ]; then
        print_line
        start_output_section
        print_line "$delta"
        end_output_section
        print_line
        print_failure_box "Initialization Test: Failed"
        print_line
        print_failure_highlight "Some useful commands:"
        print_failure_highlight "  diff -r --exclude '.git' --exclude 'doctrees' --exclude '__pycache__' '$temp_dir_link' '$expected_dir'"
        print_failure_highlight "  meld '$temp_dir_link' '$expected_dir' &"
        exit 1
    fi

    print_line
    print_success_box "Initialization test: Success"
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    print_line
    print_failure_box "Initialization test: Failed"
    print_line
    print_failure_highlight "An unexpected error has occured!"

    exit 1
}

trap error_handler ERR

main "$@"

exit 0
