#!/bin/bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TESTS_DIR="$(realpath "$SCRIPT_DIR/..")"
PROJECT_DIR="$(realpath "$TESTS_DIR/..")"

# shellcheck source=/dev/null
source "$PROJECT_DIR/bin/commons.sh"

function main() {
    print_line
    print_h1 "Testing Sphinx git-context extension"

    test_configuration "1" "dirty"
    test_configuration "2" "clean"
    test_configuration "3" "clean and tag"
    test_configuration "4" "custom function"

    print_line
    print_success_box "Sphinx git-context extension test: Success"
}

function test_configuration() {

    local test_index="$1"
    local test_title="$2"

    print_line
    print_h2 "Testing Sphinx git-context extension: $test_title"

    local temp_dir
    local temp_dir_link
    temp_dir=$(mktemp -d --suffix=.docker-sphinx-doc)
    temp_dir_link="$(dirname "$temp_dir")/docker-sphinx-doc"

    rm -rf "$temp_dir_link"
    ln -sf "$temp_dir" "$temp_dir_link"

    print_line
    print_h2_highlight "Data will be stored in the temporary directory:"
    print_h2_highlight "  $temp_dir"
    print_h2_highlight "Use the following link for convenience:"
    print_h2_highlight "  $temp_dir_link"

    print_line
    print_step "Copying default project files"

    cp -r "${TESTS_DIR}/test-init/expected/bin"          "${temp_dir}/"
    cp -r "${TESTS_DIR}/test-init/expected/source"       "${temp_dir}/"
    cp -r "${TESTS_DIR}/test-init/expected/Makefile"     "${temp_dir}/"
    cp -r "${TESTS_DIR}/test-init/expected/Makefile.bak" "${temp_dir}/"

    print_line
    print_step "Copying custom project files"

    cp -r "${TESTS_DIR}/test-git/source_${test_index}/"* "${temp_dir}/source"

    print_line
    print_step "Executing Git commands"
    print_line

    start_output_section
    pushd "${temp_dir}/" > /dev/null
    "${TESTS_DIR}/test-git/_before_generation_${test_index}.sh"
    popd > /dev/null
    end_output_section

    print_line
    print_step "Generating HTML documentation using 'make-html'"
    print_line
    start_output_section
    "$temp_dir/bin/make-html"
    end_output_section

    local build_dir="$temp_dir/build"

    if [ ! -d "$build_dir/html" ]; then
        print_line
        print_failure_box "Sphinx git-context extension test: Failed"
        print_line
        print_failure_highlight "Directory '$build_dir/html' was not created"
        exit 1
    fi

    pushd "${temp_dir}/" > /dev/null
    "${TESTS_DIR}/test-git/_before_test_${test_index}.sh"
    popd > /dev/null

    print_line
    print_step "Computing HTML differences"

    local expected_dir="$SCRIPT_DIR/expected"

    # shellcheck disable=SC2155
    local delta=$(diff -r --exclude ".buildinfo" --exclude "objects.inv" --exclude "searchindex.js" "$build_dir/html" "$expected_dir/html" || true)

    if [ "$delta" != "" ]; then
        print_line
        start_output_section
        print_line "$delta"
        end_output_section
        print_line
        print_failure_box "Sphinx git-config extension test: Failed"
        print_line
        print_failure_highlight "Generated HTML is different from expected HTML"
        print_failure_highlight "Some useful commands:"
        print_failure_highlight "  diff -r --exclude 'buildinfo' --exclude 'objects.inv' --exclude 'searchindex.js' $temp_dir_link/build/html' '$expected_dir/html'"
        print_failure_highlight "  meld '$temp_dir_link/build/html' '$expected_dir/html' &"
        if [ ! -t 1 ]; then
            start_output_section
            echo "$delta"
            end_output_section
        fi
        exit 1
    fi
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    print_line
    print_failure_box "Sphinx git-context extension test: Failed"
    print_line
    print_failure_highlight "An unexpected error has occured!"

    exit 1
}

trap error_handler ERR

main "$@"

exit 0
