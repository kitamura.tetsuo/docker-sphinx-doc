#!/bin/bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value


find "./build/html" -type f -regextype posix-egrep -regex ".*\.(html|js)$" -exec \
    sed -i -E \
        -e 's/[0-9]{4}\/[0-9]{2}\/[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2} WORKING VERSION/__EXPECTED_RELEASE__/g' \
        -e 's/[0-9]{4}\/[0-9]{2}\/[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}&lt;br\/&gt;WORKING VERSION/__EXPECTED_VERSION__/g' \
        -e 's/[0-9]{4}\/[0-9]{2}\/[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}<br\/>WORKING VERSION/__EXPECTED_VERSION__/g' \
        -e 's/[0-9]{4}\/[0-9]{2}\/[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}/__EXPECTED_LAST_UPDATED__/g' \
        {} \;
