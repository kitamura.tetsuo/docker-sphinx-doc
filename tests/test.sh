#!/bin/bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

SCRIPT_DIR="$( cd "$( dirname "$(readlink -f "${BASH_SOURCE[0]}")" )" && pwd )"
PROJECT_DIR="$(realpath "$SCRIPT_DIR/..")"

# shellcheck source=/dev/null
source "$PROJECT_DIR/bin/commons.sh"

"$SCRIPT_DIR/test-image/test.sh"
"$SCRIPT_DIR/test-init/test.sh"
"$SCRIPT_DIR/test-build/test-html.sh"
"$SCRIPT_DIR/test-build/test-pdf.sh"
"$SCRIPT_DIR/test-git/test.sh"

print_line
print_success_box "Build & Test: Success"
