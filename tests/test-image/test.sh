#!/bin/bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TESTS_DIR="$(realpath "$SCRIPT_DIR/..")"
PROJECT_DIR="$(realpath "$TESTS_DIR/..")"

# shellcheck source=/dev/null
source "$PROJECT_DIR/bin/commons.sh"

function main() {

    print_line
    print_h1 "Testing Docker image"

    print_line
    print_step "Getting Docker image metadata"

    local docker_image_sha
    local docker_image_tag
    docker_image_sha=$(docker run -e USER_ID=$UID "ddidier/sphinx-doc:latest" bash -c 'echo "$GIT_COMMIT_SHA"')
    docker_image_tag=$(docker run -e USER_ID=$UID "ddidier/sphinx-doc:latest" bash -c 'echo "$GIT_COMMIT_TAG"')
    print_line
    print_line "  - Docker image hash = $docker_image_sha"
    print_line "  - Docker image tag  = $docker_image_tag"

    print_line
    print_step "Getting Git repository metadata"

    local git_commit_sha
    local git_commit_tag
    git_commit_sha=$(git log -1 --pretty=%H)
    git_commit_tag=$(git describe --exact-match --tags "$git_commit_sha" 2> /dev/null || echo "tested")
    print_line
    print_line "  - Git repository hash = $git_commit_sha"
    print_line "  - Git repository tag  = $git_commit_tag"

    if [ "$docker_image_sha" != "$git_commit_sha" ]; then
        print_line
        print_failure_box "Docker image Test: Failed"
        print_line
        print_failure_highlight "Wrong or missing commit hash:"
        print_failure_highlight "- in Git repository = $git_commit_sha"
        print_failure_highlight "- in Docker image   = $docker_image_sha"
        print_failure_highlight "If the image was just rebuilt, try to run the tests again."
        exit 1
    fi

    if [ "$docker_image_tag" != "$git_commit_tag" ]; then
        print_line
        print_failure_box "Docker image Test: Failed"
        print_line
        print_failure_highlight "Wrong or missing commit tag:"
        print_failure_highlight "- in Git repository = $git_commit_tag"
        print_failure_highlight "- in Docker image   = $docker_image_tag"
        exit 1
    fi

    print_line
    print_success_box "Docker image test: Success"
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    print_line
    print_failure_box "Docker image test: Failed"
    print_line
    print_failure_highlight "An unexpected error has occured!"

    exit 1
}

trap error_handler ERR

main "$@"

exit 0
