#!/bin/bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TESTS_DIR="$(realpath "$SCRIPT_DIR/..")"
PROJECT_DIR="$(realpath "$TESTS_DIR/..")"

# shellcheck source=/dev/null
source "$PROJECT_DIR/bin/commons.sh"

function main() {

    docker tag ddidier/sphinx-doc:latest ddidier/sphinx-doc:testing

    print_line
    print_h1 "Testing HTML generation of a Sphinx project"

    local temp_dir
    local temp_dir_link
    temp_dir=$(mktemp -d --suffix=.docker-sphinx-doc)
    temp_dir_link="$(dirname "$temp_dir")/docker-sphinx-doc"

    rm -rf "$temp_dir_link"
    ln -sf "$temp_dir" "$temp_dir_link"

    print_line
    print_h1_highlight "Data will be stored in the temporary directory:"
    print_h1_highlight "  $temp_dir"
    print_h1_highlight "Use the following link for convenience:"
    print_h1_highlight "  $temp_dir_link"

    print_line
    print_step "Getting Docker image metadata"

    local docker_image_sha
    local docker_image_tag
    docker_image_sha=$(docker run -e USER_ID=$UID "ddidier/sphinx-doc:latest" bash -c 'echo "$GIT_COMMIT_SHA"')
    docker_image_tag=$(docker run -e USER_ID=$UID "ddidier/sphinx-doc:latest" bash -c 'echo "$GIT_COMMIT_TAG"')
    print_line
    print_line "  - Docker image hash = $docker_image_sha"
    print_line "  - Docker image tag  = $docker_image_tag"

    print_line
    print_step "Copying default project files"

    cp -r "${TESTS_DIR}/test-init/expected/bin"          "${temp_dir}/"
    cp -r "${TESTS_DIR}/test-init/expected/source"       "${temp_dir}/"
    cp -r "${TESTS_DIR}/test-init/expected/Makefile"     "${temp_dir}/"
    cp -r "${TESTS_DIR}/test-init/expected/Makefile.bak" "${temp_dir}/"

    print_line
    print_step "Copying custom project files"

    cp -r "${TESTS_DIR}/test-build/source/"* "${temp_dir}/source"

    print_line
    print_step "Generating HTML documentation using 'make-html'"
    print_line
    start_output_section
    "$temp_dir/bin/make-html"
    end_output_section

    local build_dir="$temp_dir/build"

    if [ ! -d "$build_dir/html" ]; then
        print_line
        print_failure_box "HTML generation test: Failed"
        print_line
        print_failure_highlight "Directory '$build_dir/html' was not created"
        exit 1
    fi

    print_line
    print_step "Computing HTML differences"

    local expected_dir="$SCRIPT_DIR/expected"

    # Test all files except images
    # shellcheck disable=SC2155
    local delta=$(diff -r --exclude "_images" "$build_dir/html" "$expected_dir/html" || true)

    if [ "$delta" != "" ]; then
        print_line
        start_output_section
        print_line "$delta"
        end_output_section
        print_line
        print_failure_box "HTML generation test: Failed"
        print_line
        print_failure_highlight "Generated HTML is different from expected HTML"
        print_failure_highlight "Some useful commands:"
        print_failure_highlight "  diff -r --exclude '_images' '$temp_dir_link/build/html' '$expected_dir/html'"
        print_failure_highlight "  meld '$temp_dir_link/build/html' '$expected_dir/html' &"
        exit 1
    fi

    print_line
    print_step "Computing images differences"

    # Test all images using their name
    local actual_image_names
    local expected_image_names
    actual_image_names=$(find "$build_dir/html/_images" -exec basename {} \; | sort)
    expected_image_names=$(find "$expected_dir/html/_images" -exec basename {} \; | sort)

    if [ "$actual_image_names" != "$expected_image_names" ]; then
        print_line
        print_failure_box "HTML Generation Test: Failed"
        print_line
        print_failure_highlight "Generated images are different from expected images"
        print_failure_highlight "Actual:"
        print_failure_highlight "  $actual_image_names"
        print_failure_highlight "Expected:"
        print_failure_highlight "  $expected_image_names"
        exit 1
    fi

    print_line
    print_success_box "HTML generation test: Success"
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    print_line
    print_failure_box "HTML generation test: Failed"
    print_line
    print_failure_highlight "An unexpected error has occured!"

    exit 1
}

trap error_handler ERR

main "$@"

exit 0
