#!/bin/bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TESTS_DIR="$(realpath "$SCRIPT_DIR/..")"
PROJECT_DIR="$(realpath "$TESTS_DIR/..")"

# shellcheck source=/dev/null
source "$PROJECT_DIR/bin/commons.sh"

function main() {

    docker tag ddidier/sphinx-doc:latest ddidier/sphinx-doc:testing

    print_line
    print_h1 "Testing PDF generation of a Sphinx project"

    local temp_dir
    local temp_dir_link
    temp_dir=$(mktemp -d --suffix=.docker-sphinx-doc)
    temp_dir_link="$(dirname "$temp_dir")/docker-sphinx-doc"

    rm -rf "$temp_dir_link"
    ln -sf "$temp_dir" "$temp_dir_link"

    print_line
    print_h1_highlight "Data will be stored in the temporary directory:"
    print_h1_highlight "  $temp_dir"
    print_h1_highlight "Use the following link for convenience:"
    print_h1_highlight "  $temp_dir_link"

    print_line
    print_step "Getting Docker image metadata"

    local docker_image_sha
    local docker_image_tag
    docker_image_sha=$(docker run -e USER_ID=$UID "ddidier/sphinx-doc:latest" bash -c 'echo "$GIT_COMMIT_SHA"')
    docker_image_tag=$(docker run -e USER_ID=$UID "ddidier/sphinx-doc:latest" bash -c 'echo "$GIT_COMMIT_TAG"')
    print_line
    print_line "  - Docker image hash = $docker_image_sha"
    print_line "  - Docker image tag  = $docker_image_tag"

    print_line
    print_step "Copying default project files"

    cp -r "${TESTS_DIR}/test-init/expected/bin"          "${temp_dir}/"
    cp -r "${TESTS_DIR}/test-init/expected/source"       "${temp_dir}/"
    cp -r "${TESTS_DIR}/test-init/expected/Makefile"     "${temp_dir}/"
    cp -r "${TESTS_DIR}/test-init/expected/Makefile.bak" "${temp_dir}/"

    print_line
    print_step "Copying custom project files"

    cp -r "${TESTS_DIR}/test-build/source/"* "${temp_dir}/source"

    print_line
    print_step "Generating PDF documentation using 'make-pdf'"
    print_line
    start_output_section
    "$temp_dir/bin/make-pdf"
    end_output_section

    local build_dir="$temp_dir/build"

    if [ ! -d "$build_dir/latex" ]; then
        print_line
        print_failure_box "PDF generation test: Failed"
        print_line
        print_failure_highlight "Directory '$build_dir/latex' was not created"
        exit 1
    fi

    local expected_pdf_file="$build_dir/latex/ndddockersphinxtest.pdf"

    if [ -f "$expected_pdf_file" ]; then
        print_line
        print_information_box "PDF generation test: No way to test the generated document"
        print_line
        print_information_highlight "The PDF was generated in '$expected_pdf_file'"
        print_information_highlight "Please have a look!"
    else
        print_line
        print_failure_box "PDF generation test: Failed"
        print_line
        print_failure_highlight "The expected PDF '$expected_pdf_file' was not found"
        exit 1
    fi

    print_line
    print_success_box "PDF generation test: Success"
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    print_line
    print_failure_box "PDF generation test: Failed"
    print_line
    print_failure_highlight "An unexpected error has occured!"

    exit 1
}

trap error_handler ERR

main "$@"

exit 0
